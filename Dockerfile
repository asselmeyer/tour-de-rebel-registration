FROM maven:3.6-jdk-11 as build

COPY ./backend/ /backend/
WORKDIR /backend
RUN mvn clean install

FROM openjdk:11 as production

COPY --from=build /backend/ /backend/
WORKDIR /backend

CMD [ "sleep 30 && /backend/target/backend-0.0.1-SNAPSHOT.jar" ]

