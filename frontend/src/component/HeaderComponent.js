import React, {useContext} from 'react'
import UserContext from "../service/UserContext";
import RestService from "../service/RestService";
import {useHistory} from "react-router";
import {Link} from "react-router-dom";

function HeaderComponent() {
    const userContext = useContext(UserContext);
    let history = useHistory();

    const logout = ()=>{
        RestService.logout();
        userContext.setUser(null);
    };

    return (<div className="Main">
        <div className="App-header">
            <h2>Welcome to React</h2>
            {userContext.user ?
            <div>
                <p>Angemedet als:{userContext.user.firstName}<input defaultValue="Logout" type="Button" onClick={logout}/></p>
                <p>
                    <Link to="/teams">Teams</Link>&nbsp;&nbsp;&nbsp;
                    <Link to="/account">Account</Link>&nbsp;&nbsp;&nbsp;
                </p>
            </div>:
            <div>
                <input defaultValue="Login" type="button" onClick={()=>history.push("/login")}/>
                <input value="Registrieren" type="button" onClick={()=>history.push("/registration")}/>
            </div>}
        </div>
    </div>);
}

export default HeaderComponent;