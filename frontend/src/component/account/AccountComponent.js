import React,{useContext,useState,useEffect} from "react";
import UserContext from "../../service/UserContext";
import RestService from "../../service/RestService";
import AccountTeamListComponent from "./AccountTeamListComponent";

function AccountComponent() {

    const userContext = useContext(UserContext);
    const [attendees, setAttendees] = useState(null);

    useEffect(()=>{
        setAttendees(userContext.user)
    },[userContext]);

    const [countries,setCountries] = useState(null);
    const [organisations,setOrganisations] = useState(null);

    useEffect(()=>{
        RestService.getCountries().then((res)=>{
            setCountries(res);
        });
        RestService.getOrganisations().then((res)=>{
            setOrganisations(res);
        });
    },[]);

    const edit = (param, value) => {
        let att = {...attendees};
        att[param] = value;
        setAttendees(att);
    };

    const ChangeButton = () => (
        <button
            type='button'
            onClick={() => {
                RestService.saveAttendees(attendees).then(res=>{
                    setAttendees(res);
                    alert("Änderrungen gespeichert")
                }).catch(err=>{
                    err.response.json().then((text)=>{
                        alert(text.message);
                    });
                });
            }}
        >
            Speichern
        </button>
    );

    //console.log(attendees);
    //console.log(countries);

    const ChangePasswordButton = () => (
        <button
            type='button'
            onClick={() => {
            }}
        >
            Passwort ändern
        </button>
    );

    return(
        <div className="Registration">
            <h2>Account bearbeiten</h2>
            {attendees && countries && organisations && <div>
                <p>
                    Username: {attendees.username}
                </p>
                <p>
                    First Name: {attendees.firstName}
                </p>
                <p>
                    Second Name: {attendees.secondName}
                </p>
                <p>
                    E-Mail: {attendees.mail}
                </p>
                <p>
                    Phone:
                    <input value={attendees.phone} onChange={(ev) => edit("phone", ev.target.value)}/>
                </p>

                <p>
                    Land auswählen:&nbsp;
                    <select id="countries" onChange={(ev) => edit("country", countries.find((c)=>c.name===ev.target.value).id)}>
                        {countries.map((country,i)=>{
                            if(attendees.country === country.id) {
                                return <option selected key={i}>{country.name}</option>
                            }else{
                                return <option key={i}>{country.name}</option>
                            }
                        })}
                    </select>
                </p>

                <p>
                    Postleitzahl: {attendees.plz}
                    <input value={attendees.plz} onChange={(ev) => edit("plz", ev.target.value)}/>
                </p>

                <p>
                    Organisation auswählen:&nbsp;
                    <select id="organisations" selected={attendees.organisation} onChange={(ev) => edit("organisation", organisations.find((c)=>c.name===ev.target.value).id)}>
                        {organisations.map((organisation,i)=>{
                            if(organisation.id === attendees.organisation) {
                                return <option selected key={i}>{organisation.name}</option>
                            }else{
                                return <option key={i}>{organisation.name}</option>
                            }
                        })}
                    </select>
                </p>

                <p>
                    <div>So unterstütze ich die Tour</div>
                    <div><input type="checkbox" checked={attendees.isCycling} onChange={(ev) => edit("isCycling", ev.target.value)}/> Ich fahre mit</div>
                    <div><input type="checkbox" checked={attendees.isPress} onChange={(ev) => edit("isPress", ev.target.value)}/> Ich bin von der Presse</div>
                    <div><input type="checkbox" checked={attendees.isOrga} onChange={(ev) => edit("isOrga", ev.target.value)}/> Ich unterstütze bei der Orga</div>
                </p>
                <ChangeButton/>
                <ChangePasswordButton/>
                <AccountTeamListComponent/>
            </div>}
        </div>)
}

export default AccountComponent;