import React, {useEffect,useState} from 'react'
import RestService from "../../service/RestService";
import {Link} from "react-router-dom";

function AccountTeamListComponent() {

    const [teams,setTeams] = useState(null);

    useEffect(()=>{RestService.getOwnTeams().then(setTeams);},[]);

    const TeamList=(props)=><div>
        <ul>
            {props.teams.map((v,k)=>{
            return <li key={k}>
                {v.name}
                <Link to={"/team/"+v.id}>Betrachten</Link>
            </li>
        })}
        </ul>
    </div>;

    return <div className="accountTeamList">
        <h2>Teams</h2>
        {teams && (teams.length === 0 ?
            <div>Keine eigenen Teams vorhanden</div>
        :
            <TeamList teams={teams}/>)
        }
    </div>
}

export default AccountTeamListComponent;