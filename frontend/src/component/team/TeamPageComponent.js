import React,{useState,useEffect} from "react";
import RestService from "../../service/RestService";
import {Link} from "react-router-dom";

let localInited = false;

function TeamPageComponent() {

    const [searchTerm,setSearchTerm] = useState("");
    const [timoutHandler,setTimeoutHandler] = useState(null);
    const [teams,setTeams] = useState([]);
    const [page,setPage] = useState(1);
    const [maxPages,setMaxPages] = useState(1);
    const [loading,setLoading] = useState(true);

    useEffect(()=>{
        if(!localInited){
            return;
        }
        if(timoutHandler !== null){
            window.clearTimeout(timoutHandler);
        }
        setTeams([]);
        setLoading(true);
        setTimeoutHandler(setTimeout(()=> {
            if (page !== 1) {
                setPage(1);//will reload view
            } else {
                RestService.findTeams(searchTerm, page).then(res => {
                    setTeams(res.elements);
                    setMaxPages(res.totalPages);
                    setLoading(false);
                }).catch(() => {
                    setLoading(false);
            });
        }
        },1000))
    },[searchTerm]);

    useEffect(()=>{
        setLoading(true);
        setTeams([]);
        console.log("increase page");
        if(timoutHandler !== null){
            window.clearTimeout(timoutHandler);
            setTimeoutHandler(null);
        }
        RestService.findTeams(searchTerm,page).then(res=>{
            console.log(res);
            setTeams(res.elements);
            setMaxPages(res.totalPages);
            setLoading(false);
            localInited = true;
        }).catch(()=>{
            localInited = true;
            setLoading(false);
        });
    },[page]);

    return(<div className="teamPage">
        Filtern nach name: <input type="text" onChange={(ev)=>setSearchTerm(ev.target.value)}/>

        {loading ? <div>Loading...</div> :
        <ul>
            {teams.map((v,k)=>{
                return <li key={k}>{v.name} <Link to={"/team/"+v.id}>Anzeigen</Link></li>
            })}
        </ul>}

        {page > 1 && <input value="Zurück" type="button" onClick={(ev)=>setPage(page-1)}/>}
        &nbsp;Seite: {page}&nbsp;
        {maxPages > page && <input value="Weiter" type="button" onClick={(ev)=>setPage(page+1)}/>}

    </div>);
}

export default TeamPageComponent;