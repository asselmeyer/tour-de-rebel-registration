import decode from 'jwt-decode';

class RestService {

    static url = window.location.href+"api";

    static initUrl(){
        this.url = window.location.href;
        let arr = this.url.split("//");
        console.log(arr);
        this.url = arr[0]+"//"+arr[1].split("/")[0];
        if(!this.url.endsWith("/")){
            this.url+="/api";
        }
        console.log("Url is: "+this.url);
    }

    static createAttendees(attendees) {
        return this.fetch(this.url + "/attendees/create", {
            method: 'POST',
            body: JSON.stringify(attendees)
        }).then(res => {
            this.setToken(res.token);
            return Promise.resolve(res);
        });
    }

    static saveAttendees(attendees) {
        return this.fetch(this.url + "/attendees", {
            method: 'POST',
            body: JSON.stringify(attendees)
        }).then(res => {
            this.setToken(res.token);
            return Promise.resolve(res);
        });
    }

    static login(credentials) {
        return this.fetch(this.url + "/authenticate", {
            method: 'POST',
            body: JSON.stringify(credentials)
        }).then(res => {
            this.setToken(res.token);
            return Promise.resolve(res);
        });
    }

    static createTeam(team){
        return this.fetch(this.url+"/team/create", {
            method: 'POST',
            body: JSON.stringify(team)
        });
    }

    static updateTeam(team){
        return this.fetch(this.url+"/team/"+team.id, {
            method: 'POST',
            body: JSON.stringify(team)
        });
    }


    static findTeams(searchTerm,page){
        let url = this.url+"/team/find/"+page;
        if(searchTerm && searchTerm !== "") {
            url+="/"+encodeURIComponent(searchTerm);
        }
        return this.fetch(url, {
            method: 'GET'
        })
    }

    static getUser() {
        return this.fetch(this.url + "/attendees", {
            method: 'GET'
        })
    }

    static getTeam(id) {
        return this.fetch(this.url + "/team/"+id, {
            method: 'GET'
        })
    }

    static leaveTeam(id) {
        return this.fetch(this.url + "/team/"+id+"/leave", {
            method: 'POST'
        })
    }

    static joinTeam(id) {
        return this.fetch(this.url + "/team/"+id+"/join", {
            method: 'POST'
        })
    }

    static kickTeam(id,attendeesId) {
        return this.fetch(this.url + "/team/"+id+"/kick/"+attendeesId, {
            method: 'POST'
        })
    }

    static makeTeamLeader(id,attendeesId) {
        return this.fetch(this.url + "/team/"+id+"/leader/"+attendeesId, {
            method: 'POST'
        })
    }

    static getOwnTeams() {
        return this.fetch(this.url + "/attendees/teams", {
            method: 'GET'
        })
    }

    static getOrganisations() {
        return this.fetch(this.url + "/organisations", {
            method: 'GET'
        })
    }

    static getCountries() {
        return this.fetch(this.url + "/countries", {
            method: 'GET'
        })
    }

    static loggedIn() {
        // Checks if there is a saved token and it's still valid
        const token = this.getToken();// GEtting token from localstorage
        return !!token && !this.isTokenExpired(token) // handwaiving here
    }

    static isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) { // Checking if token is expired. N
                return true;
            } else
                return false;
        } catch (err) {
            return false;
        }
    }

    static setToken(idToken) {
        // Saves user token to localStorage
        localStorage.setItem('id_token', idToken);
        //console.log(localStorage.getItem('id_token'));
    }

    static getToken() {
        // Retrieves the user token from localStorage
        return localStorage.getItem('id_token')
    }

    static logout() {
        // Clear user token and profile data from localStorage
        localStorage.removeItem('id_token');
    }

    static getProfile() {
        // Using jwt-decode npm package to decode the token
        return decode(this.getToken());
    }

    static fetch(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        // Setting Authorization header
        // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

    static _checkStatus(response) {
        // raises an error in case response status is not a success
        if (response.status >= 200 && response.status < 300) { // Success status lies between 200 to 300
            return response
        } else {
            var error = new Error(response.statusText);
            error.response = response;
            throw error
        }
    }
}

export default RestService;