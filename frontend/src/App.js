import React, {useState,useEffect} from 'react'
import {Route, Router} from "react-router";
import HeaderComponent from "./component/HeaderComponent";
import LoginComponent from "./component/LoginComponent";
import RegistrationComponent from "./component/RegistrationComponent";
import createBrowserHistory from 'history/createBrowserHistory'
import {UserProvider} from "./service/UserContext";
import RestService from "./service/RestService";
import TeamsComponent from "./component/team/TeamsComponent";
import AccountComponent from "./component/account/AccountComponent";
import TeamDetailComponent from "./component/team/TeamDetailComponent";

const newHistory = createBrowserHistory();

function App() {

    RestService.initUrl();

    const [user, setUser] = useState(null);
    const [token, setToken] = useState( RestService.getToken());

    const userProvider = {
        user: user,
        setUser: setUser,
        token: token,
        setToken: setToken
    };

    const fetchUser = () => {
        RestService.getUser().then(res => {
            setUser(res);
        }).catch(err=>{
            if(err.response.status === 401){
                resetLogin();
            }
        });
    };

    const resetLogin = ()=>{
        RestService.logout();
        setToken(null);
    };

    useEffect(()=> {
        if (!user) {
            if (token){
                if( !RestService.isTokenExpired(token)) {
                    fetchUser();
                }else {
                    resetLogin();
                }
            }
        } else {
            if (!token || RestService.isTokenExpired(token)) {
                setUser(null);
            }
        }
    },[token]);

    return (<UserProvider value={userProvider}>
        <Router history={newHistory}>
            <Route path="/" component={HeaderComponent}/>
            <Route path="/login" component={LoginComponent}/>
            <Route path="/registration" component={RegistrationComponent}/>
            <Route path="/teams" component={TeamsComponent}/>
            <Route path="/account" component={AccountComponent}/>
            <Route path="/team/:id" component={TeamDetailComponent}/>
        </Router>
    </UserProvider>)
}

export default App;