#!/usr/bin/env sh

set -ex

CI_COMMIT_SHA='0.0.2'
DOCKER_IMAGE=${DOCKER_IMAGE-'local'}
if [ "${DOCKER_IMAGE}" != 'local' ]
then
    # try to pull previously built images to utilize docker cache during new build
    docker pull $DOCKER_IMAGE/production:latest || true
fi;


# build and tag intermediate images and application image
docker build . --target production -t $DOCKER_IMAGE/production:$CI_COMMIT_SHA
docker build . --target production -t $DOCKER_IMAGE/production:$CI_COMMIT_SHA
docker push $DOCKER_IMAGE/production:$CI_COMMIT_SHA