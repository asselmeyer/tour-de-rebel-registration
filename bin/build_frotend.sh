docker run -it --rm --name my-maven-project-certchecker \
  -v /home/tost/workspace/certstream-watcher/frontend:/proj \
  -w /proj \
  node:8.16	 \
  /bin/bash -c "npm install && npm run build"
