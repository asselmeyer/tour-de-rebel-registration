package org.tdr.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class RegistrationAttendessDTO {
    private String username;
    private String password;
    private String mail;

    private String firstName;
    private String secondName;
    private String phone;
    private String plz;

    private Boolean isCycling;
    private Boolean isPress;
    private Boolean isOrga;

    private Long organisation;
    private Long country;
}
