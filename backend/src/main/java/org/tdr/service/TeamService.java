package org.tdr.service;

import org.mockito.internal.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.jaxb.SpringDataJaxb;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;
import org.tdr.dto.TeamDTO;
import org.tdr.model.Attendees;
import org.tdr.model.Team;
import org.tdr.repository.AttendeesRespository;
import org.tdr.repository.TeamRepository;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class TeamService {

    @Autowired
    private AttendeesRespository attendeesRespository;

    @Autowired
    private TeamRepository teamRepository;

    @Value("${team.page-size:10}")
    private int pageSize;

    public Team createTeam(TeamDTO teamDTO, Attendees attendees){
        Team team = Team.builder().leader(attendees).description(teamDTO.getDescription()).members(Collections.singletonList(attendees)).name(teamDTO.getName()).build();
        try {
            team = teamRepository.save(team);
        }catch(Exception ex){
            System.out.println(ex);
            return null;
        }
        return team;
    }

    public Team updateTeam(TeamDTO teamDTO, Attendees attendees){
        Team team = teamRepository.findById((long)teamDTO.getId());
        if(team == null){
            return null;
        }
        if(!team.getLeader().getId().equals(attendees.getId())){
            return null;
        }
        team.setName(teamDTO.getName());
        team.setDescription(teamDTO.getDescription());
        return teamRepository.save(team);
    }

    public Team getTeam(long id){
        return teamRepository.findById(id);
    }

    public Page<Team> findTeams(String searchTerm,int page){
        PageRequest pageRequest = PageRequest.of(page,pageSize);
        if(StringUtils.isEmpty(searchTerm)){
            return teamRepository.findAll(pageRequest);
        }
        return teamRepository.findAllByNameContaining(searchTerm,pageRequest);
    }

    public Team joinTeam(long id,Attendees attendees){
        Team team = teamRepository.findById(id);
        if(team == null){
            return null;
        }
        if(team.getMembers().stream().anyMatch((v)->v.getId().equals(attendees.getId()))){
            return null;
        }
        team.getMembers().add(attendees);//TODO replace by only insert command
        team = teamRepository.save(team);
        return team;
    }

    public Team leaveTeam(long id,Attendees attendees){
        Team team = teamRepository.findById(id);
        if(team == null){
            return null;
        }
        if(team.getMembers().stream().noneMatch((v)->v.getId().equals(attendees.getId()))){
            return null;
        }
        if(team.getLeader().getId().equals(attendees.getId())){
            return null;
        }
        team.getMembers().removeIf((v)->v.getId().equals(attendees.getId()));//TODO replace by only insert command
        team = teamRepository.save(team);
        return team;
    }

    public Team kickFromTeam(long id,Attendees attendees,long attendeesid){
        Team team = teamRepository.findById(id);
        if(team == null){
            return null;
        }
        if(!team.getLeader().getId().equals(attendees.getId())){
            return null;//TODO make good error handling -> access denied
        }
        if(attendees.getId() == attendeesid){
            return null;
        }
        if(team.getMembers().stream().noneMatch((v)->v.getId().equals(attendeesid))){
            return null;
        }
        team.getMembers().removeIf((v)->v.getId().equals(attendeesid));//TODO replace by only insert command
        team = teamRepository.save(team);
        return team;
    }
    public Team makeNewLeader(long id,Attendees attendees,long attendeesid){
        Team team = teamRepository.findById(id);
        if(team == null){
            return null;
        }
        if(!team.getLeader().getId().equals(attendees.getId())){
            return null;//TODO make good error handling -> access denied
        }
        if(attendees.getId() == attendeesid){
            return null;
        }
        Optional<Attendees> newLeader = team.getMembers().stream().filter((v)->v.getId().equals(attendeesid)).findFirst();
        if(newLeader.isEmpty()){
            return null;
        }
        team.setLeader(newLeader.get());
        team = teamRepository.save(team);
        return team;
    }

    public List<Team> getTeamsByLeader(Attendees attendees){
        return teamRepository.findAllByLeaderId(attendees.getId());
    }
}
